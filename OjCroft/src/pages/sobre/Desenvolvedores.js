import React from 'react';
import { View, Text } from 'react-native';
import styled from 'styled-components/native';

export default () => {
    return (

        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#1C1C1C' }}>
            <View style={{ alignItems: 'center' }}>
                <CardLogo source={require('../../../assets/logo.png')}></CardLogo>
            </View>

            <Nome>Yusterley Almeida</Nome>
            <Nome>Orlando Oliveira</Nome>
            <Nome>Fábio Sousa</Nome>
        </View>
    );
}


const Nome = styled.Text`
    margin: 20px;
    font-size: 40px; 
    color: #FFD700; 
    font-weight: bold;
`;
const CardLogo = styled.ImageBackground`
  height: 100px;
  width: 100px;
  margin-top: 20px;
`;

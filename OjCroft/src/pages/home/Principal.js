import React from 'react';
import { View, ScrollView, TouchableOpacity, Button } from 'react-native';
import styled from 'styled-components/native';


export default (props) => {
  const { navigation } = props;

  function handlebutton() {
    navigation.navigate('primeira')
  };
  function handlebutton2() {
    navigation.navigate('segunda')
  };
  function handlebutton3() {
    navigation.navigate('terceira')
  };
  function handlebutton4() {
    navigation.navigate('quarta')
  };
  function desenvolvedores() {
    navigation.navigate('desenvolvedores')
  };

  return (
    <View style={{backgroundColor: '#1C1C1C', height: '100%'}}>
     
      <ScrollView>

        <View style={{alignItems: 'center'}}>
          <CardLogo source={require('../../../assets/logo.png')}></CardLogo>
        </View>
        
        <TituloNoticias>| Tecnologia/Jogos</TituloNoticias>

        <TouchableOpacity onPress={handlebutton}>
          <CardNoticia source={{ uri: 'https://i.imgur.com/MfRi6qA.jpg' }}>
            <TituloCard>A tremenda placa de vídeo chegou, veja às características dela.</TituloCard>
          </CardNoticia>
        </TouchableOpacity>

        <TouchableOpacity onPress={handlebutton2}>
          <CardNoticia source={require('../../../assets/segunda.jpg')}>
            <TituloCard>Araní: Jogo brasileiro que chamou a atenção do mundo ganhará novidades em breve</TituloCard>
          </CardNoticia>
        </TouchableOpacity>

        <TituloNoticias>| Filmes/Animes/Séries</TituloNoticias>

        <TouchableOpacity onPress={handlebutton3}>
          <CardNoticia source={{ uri: 'https://i.imgur.com/xivoUpq.jpg' }}>
            <TituloCard>Filme do Borderlands revela silhueta dos personagens principais</TituloCard>
          </CardNoticia>
        </TouchableOpacity>

        <TouchableOpacity onPress={handlebutton4}>
          <CardNoticia source={{ uri: 'https://i.imgur.com/vrYEyve.jpg' }}>
            <TituloCard>11 filmes de terror para assistir em 2021</TituloCard>
          </CardNoticia>
        </TouchableOpacity>

        <TouchableOpacity onPress={desenvolvedores} style={{alignItems: 'center'}}>
          <Desenvolvedores>| DESENVOLVEDORES |</Desenvolvedores>
        </TouchableOpacity>

      </ScrollView>
    </View>
  );
}


const TituloNoticias = styled.Text`
  margin: 30px 0px 0px 15px;
  font-size: 30px; 
  color: #00BFFF; 
  font-weight: bold;
`;
const TituloCard = styled.Text`
  color: #FFF;
  font-size: 20px;
  font-weight: bold;
  background-color: #000000a0;
`;

const CardNoticia = styled.ImageBackground`
  height: 200px;
  margin: 16px;
  justify-content: flex-end;
  padding: 10px;
  border: 1px solid #fff;
`;

const CardLogo = styled.ImageBackground`
  height: 100px;
  width: 100px;
  margin-top: 20px;
`;
const Desenvolvedores = styled.Text`
  margin-bottom: 20px;
  margin-top: 20px;
  font-size: 30px; 
  color: #FFD700; 
  font-weight: bold;
`;
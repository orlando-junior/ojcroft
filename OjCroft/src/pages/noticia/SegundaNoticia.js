import React from 'react';
import { View, ScrollView, } from 'react-native';
import styled from 'styled-components/native';

export default () => {
    return(
        
        <View>
            <ScrollView>
                
                <CardNoticia 
                  source={require('../../../assets/segunda.jpg')}>
                </CardNoticia>
                
                <View style={{alignItems: 'center',}}>

                    <Titulo>Araní: Jogo brasileiro que chamou a atenção do mundo ganhará novidades em breve</Titulo>
                    <Paragrafo>
                    Ainda em desenvolvimento, jogo brasileiro da Diorama Digital pode receber novidades em breve
                    </Paragrafo>
                    <Paragrafo>
                    O mundo dos games já nos permitiu controlar guerreiros dos mais variados tipos — centuriões, deuses, cowboys, samurais, etc. — mas poucas vezes tivemos a chance de controlarmos guerreiros indígenas. Araní, uma produção do estúdio independente Diorama Digital, é um jogo indie brasileiro que planeja reverter esta situação. Anunciado em novembro de 2018, a produção permaneceu em silêncio, com alguns fãs, inclusive, achando que o título havia sido cancelado. Felizmente, este não é o caso.
                    </Paragrafo>
                    <Paragrafo>
                    Apesar do jogo ainda não ter uma previsão de lançamento, Everaldo Neto, Co-Fundador e Diretor de Arte da Diorama Digital, afirmou em entrevista ao Combo Infinito que Araní poderá receber novidades ainda este ano. Embora mais detalhes não tenham sido revelados por Neto, ele afirma que a falta de notícias sobre o título se deve por conta de publishers, que estão atualmente disputando pelo título. Este, no entanto, tornou-se o principal motivo do "sumiço" de aproximadamente dois anos do estúdio — um silêncio que, para muitos, acabou compreendido como um cancelamento do projeto.
                    </Paragrafo>
                </View>
                
            </ScrollView>
        </View>
    );
}


const CardNoticia = styled.ImageBackground`
    height: 200px;
`;
const Titulo = styled.Text`
    font-size: 30px;
    font-weight: bold;

`;
const Paragrafo = styled.Text`
    font-size: 18px;
    text-align: left;
    margin-bottom: 10px;
    margin-top: 10px;
    width: 90%;
`;
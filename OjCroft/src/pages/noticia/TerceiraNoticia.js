import React from 'react';
import { View,ScrollView } from 'react-native';
import styled from 'styled-components/native';

export default () => {
    return(
        
        <View>
            <ScrollView>
                
                <CardNoticia 
                  source={{ uri: 'https://i.imgur.com/xivoUpq.jpg' }}>
                </CardNoticia>
                
                <View style={{alignItems: 'center',}}>

                    <Titulo>Filme do Borderlands revela silhueta dos personagens principais</Titulo>
                    <Paragrafo>
                        Borderlands receberá em breve uma adaptação para as telonas, trazendo os personagens favoritos de todos para o cinema. A essa altura, todos os personagens principais já tiveram seus atores escalados.
                    </Paragrafo>
                    <Paragrafo>
                        Semana passada a Gearbox revelou que o filme será um universo à parte dos jogos, uma produção totalmente independente fora do universo dos jogos.
                    </Paragrafo>
                </View>
                
            </ScrollView>
        </View>
    );
}


const CardNoticia = styled.ImageBackground`
    height: 200px;
`;
const Titulo = styled.Text`
    font-size: 30px;
    font-weight: bold;
    margin-top: 20px;
`;
const Paragrafo = styled.Text`
    font-size: 18px;
    text-align: left;
    margin-bottom: 10px;
    margin-top: 10px;
    width: 90%;
`;

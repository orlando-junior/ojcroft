import React from 'react';
import { View,ScrollView } from 'react-native';
import styled from 'styled-components/native';

export default () => {
    return(
        
        <View>
            <ScrollView>
                
                <CardNoticia 
                  source={{ uri: 'https://i.imgur.com/vrYEyve.jpg' }}>
                </CardNoticia>
                
                <View style={{alignItems: 'center',}}>

                    <Titulo>Um Lugar Silencioso 2 | Família tenta sobreviver ao apocalipse no trailer final</Titulo>
                    <Paragrafo>
                        Um Lugar Silencioso - Parte II teve seu trailer final divulgado, que mostra a família Abbott tentando sobreviver no pós-apocalipse.
                    </Paragrafo>
                    <Paragrafo>
                        John Krasinski retorna como diretor e roteirista. Emily Blunt, Noah Jupe e Millicent Simmonds estão de volta ao elenco, que terá também Cillian Murphy (Peaky Blinders) e Djimon Hounsou
                    </Paragrafo>
                    <Paragrafo>
                        No primeiro longa, uma família tenta se manter em total silêncio para sobreviver à ameaça que ronda a sua casa e que pode atacá-los ao menor sinal de barulho. A produção arrecadou US$ 340 milhões na bilheteria para um orçamento estimado em US$ 17 milhões.
                    </Paragrafo>
                </View>
                
            </ScrollView>
        </View>
    );
}


const CardNoticia = styled.ImageBackground`
    height: 200px;
`;
const Titulo = styled.Text`
    font-size: 30px;
    font-weight: bold;
    padding: 10px;
`;
const Paragrafo = styled.Text`
    font-size: 18px;
    text-align: left;
    margin-bottom: 10px;
    margin-top: 10px;
    width: 90%;
`;

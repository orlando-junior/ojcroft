import React from 'react';
import { View,ScrollView } from 'react-native';
import styled from 'styled-components/native';

export default () => {
    return(
        
        <View>
            <ScrollView>
                
                <CardNoticia 
                  source={require('../../../assets/RTX3080TI.png')}>
                </CardNoticia>
                
                <View style={{alignItems: 'center',}}>

                    <Titulo>A tremenda placa de vídeo chegou, veja às características dela.</Titulo>
                    <Paragrafo>
                        A GeForce RTX 3080 Ti vem para ser a opção imediatamente abaixo da GeForce RTX 3090. Ambas são baseadas no chip GA102, mas o    modelo 3080 Ti traz alguns recursos a menos, como uma quantidade reduzida de memória: a RTX 3090 conta com 24 GB de DDR6X; a   RTX 3080 Ti, com 12 GB.
                    </Paragrafo>
                    <Paragrafo>
                        Apesar das diferenças em relação à RTX 3090, a RTX 3080 Ti não deixa de ser uma opção poderosa. O modelo conta com 10.240 núcleos CUDA, por exemplo, contra os 8.704 núcleos da GeForce RTX 3080 “normal”.
                    </Paragrafo>
                    <Paragrafo>
                        Na comparação com as gerações anteriores, a Nvidia fala em desempenho até duas vezes superior em relação à GeForce GTX 1080 Ti e 1,5 vez maior em relação à GeForce RTX 2080 Ti em tarefas de rasterização.
                    </Paragrafo>
                </View>
                
            </ScrollView>
        </View>
    );
}


const CardNoticia = styled.ImageBackground`
    height: 200px;
`;
const Titulo = styled.Text`
    font-size: 30px;
    font-weight: bold;

`;
const Paragrafo = styled.Text`
    font-size: 18px;
    text-align: left;
    margin-bottom: 10px;
    margin-top: 10px;
    width: 90%;
`;

import React, { useState } from 'react';
import Principal from './src/pages/home/Principal';
import Noticia from './src/pages/noticia/PrimeiraNoticia';
import SegundaNoticia from './src/pages/noticia/SegundaNoticia';

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack';
import TerceiraNoticia from './src/pages/noticia/TerceiraNoticia';
import QuartaNoticia from './src/pages/noticia/QuartaNoticia';
import Desenvolvedores from './src/pages/sobre/Desenvolvedores';

const Pilha = createStackNavigator();

export default () => {
  return (
    <NavigationContainer>
      <Pilha.Navigator>

        <Pilha.Screen
          name="home"
          component={Principal}
          options={{
            title: 'Home',
            headerTintColor: '#fff',
            headerTitleAlign: 'center',
            headerStyle: { backgroundColor: '#4F4F4F' }
          }}
        />
        <Pilha.Screen
          name="primeira"
          component={Noticia}
          options={{
            title: 'Tecnologia/Jogos',
            headerTintColor: '#fff',
            headerTitleAlign: 'center',
            headerStyle: { backgroundColor: '#4F4F4F' }
          }}
        />
        <Pilha.Screen
          name="segunda"
          component={SegundaNoticia}
          options={{
            title: 'Tecnologia/Jogos',
            headerTintColor: '#fff',
            headerTitleAlign: 'center',
            headerStyle: { backgroundColor: '#4F4F4F' }
          }}
        />
        <Pilha.Screen
          name="terceira"
          component={TerceiraNoticia}
          options={{
            title: 'Filmes/Animes/Séries',
            headerTintColor: '#fff',
            headerTitleAlign: 'center',
            headerStyle: { backgroundColor: '#4F4F4F' }
          }}
        />
        <Pilha.Screen
          name="quarta"
          component={QuartaNoticia}
          options={{
            title: 'Filmes/Animes/Séries',
            headerTintColor: '#fff',
            headerTitleAlign: 'center',
            headerStyle: { backgroundColor: '#4F4F4F' }
          }}
        />
        <Pilha.Screen
          name="desenvolvedores"
          component={Desenvolvedores}
          options={{
            title: 'Desenvolvedores',
            headerTintColor: '#fff',
            headerTitleAlign: 'center',
            headerStyle: { backgroundColor: '#4F4F4F' }
          }}
        />

      </Pilha.Navigator>
    </NavigationContainer>
  );
}